package io.bitbucket.nijvaxa;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import javafx.scene.text.Font;

public class TryTextArea extends Application {
    @Override
    public void start(Stage stage) {
        TextArea txt1 = new TextArea("नमस्ते जग्गु");
        txt1.setFont(Font.font("Serif", 30));
        txt1.setLayoutX(10);
        txt1.setLayoutY(10);
        txt1.setMaxWidth(500);
        txt1.setMaxHeight(400);

        Group root = new Group(txt1);

        Scene scn = new Scene(root, 550, 450);
        stage.setScene(scn);
        stage.show();
    }
}

