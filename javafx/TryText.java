package io.bitbucket.nijvaxa;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.text.Font;

public class TryText extends Application {
    @Override
    public void start(Stage stage) {
        Text txt1 = new Text("नमस्ते जग्गु");
        txt1.setFont(Font.font("Serif", 100));
        txt1.setLayoutX(50);
        txt1.setLayoutY(200);

        Group root = new Group(txt1);

        Scene scn = new Scene(root, 550, 450);
        stage.setScene(scn);
        stage.show();
    }
}

